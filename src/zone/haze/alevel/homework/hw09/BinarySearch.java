package zone.haze.alevel.homework.hw09;

import java.util.Arrays;

public class BinarySearch<T extends Comparable<T>> {
    private T[] whereToSearch;
    private int minimumIndex, maximumIndex, medianIndex, compare;

    public static void main(String... args) {
        BinarySearch search = new BinarySearch<>(new Integer[]{3, 1, 2, 8, 19});
        search.printIn(2);
        search.printIn(19);
        search.printIn(6);

        search = new BinarySearch<>(new String[]{"orange", "apple", "banana", "watermelon"});
        search.printIn("orange");
        search.printIn("banana");
        search.printIn("potato");
    }

    public BinarySearch(T[] input) {
        whereToSearch = input;
        Arrays.sort(whereToSearch); // Binary search can only has been provided over sorted arrays
    }

    private int index(T[] whereToSearch, T whatToSearch) {
        minimumIndex = 0;
        maximumIndex = whereToSearch.length - 1;

        while (minimumIndex <= maximumIndex) {
            medianIndex = (minimumIndex + maximumIndex) >>> 1;
            compare = whereToSearch[medianIndex].compareTo(whatToSearch);

            if (compare < 0)
                minimumIndex = medianIndex + 1;
            else if (compare > 0)
                maximumIndex = medianIndex - 1;
            else
                return medianIndex;
        }

        return -1;
    }

    public int index(T whatToSearch) {
        return index(whereToSearch, whatToSearch);
    }

    public boolean inArray(T whatToSearch) {
        return index(whatToSearch) != -1;
    }

    public void printIn(T whatToSearch) {
        System.out.println(inArray(whatToSearch));
    }

}

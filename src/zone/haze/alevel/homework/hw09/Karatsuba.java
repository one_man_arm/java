package zone.haze.alevel.homework.hw09;

import zone.haze.util.Timer;

import java.math.BigInteger;

public class Karatsuba {

    private static int max(int a, int b) {
        return a > b ? a : b;
    }

    public static BigInteger multiply(long x, long y) {
        return multiply(BigInteger.valueOf(x), BigInteger.valueOf(y));
    }

    public static BigInteger multiply(BigInteger x, BigInteger y) {
        return multiply(x, y, max(x.bitLength(), y.bitLength()));
    }

    public static BigInteger multiply(BigInteger x, BigInteger y, int len) {
        // Standard multiply
        if (len < 4) return x.multiply(y);

        int middle = len / 2;

        BigInteger x0 = x.shiftRight(middle);
        BigInteger x1 = x.subtract(x0.shiftLeft(middle));
        BigInteger y0 = y.shiftRight(middle);
        BigInteger y1 = y.subtract(y0.shiftLeft(middle));

        BigInteger z0 = multiply(x1, y1, middle);
        BigInteger z1 = multiply(x0, y0, middle);
        BigInteger z2 = multiply(x1.add(x0), y1.add(y0), middle);

        return z0.add(z2.subtract(z0).subtract(z1).shiftLeft(middle)).add(z1.shiftLeft(2 * middle));
    }

    private Karatsuba() {
        /* Util class */
    }

    public static void main(String[] args) {
        new Timer("Initialization");
        BigInteger a = new BigInteger("901234567890123456789012345678901234567890123456789")
                .multiply(new BigInteger("901234567890123456789012345678901234567890123456789"))
                .multiply(new BigInteger("901234567890123456789012345678901234567890123456789"))
                .multiply(new BigInteger("901234567890123456789012345678901234567890123456789"))
                .multiply(new BigInteger("901234567890123456789012345678901234567890123456789"))
                .multiply(new BigInteger("901234567890123456789012345678901234567890123456789"))
                .multiply(new BigInteger("901234567890123456789012345678901234567890123456789"));
        BigInteger b = new BigInteger("9876543210987654321098765432109876543210987654321098765")
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"))
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"))
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"))
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"))
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"))
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"))
                .multiply(new BigInteger("9876543210987654321098765432109876543210987654321098765"));
        System.out.printf("Initialization cost: %d ms\n", Timer.stop());

        new Timer("Karatsuba");
        BigInteger result1 = Karatsuba.multiply(a, b);
        System.out.printf("Karatsuba: %d ms\n", Timer.stop("Karatsuba"));

        new Timer("Not Karatsuba");
        // This one is always faster :c
        BigInteger result2 = a.multiply(b);
        System.out.printf("Not Karatsuba: %d ms\n", Timer.stop("Not Karatsuba"));

        System.out.printf("Results are %s!\n", result1.equals(result2) ? "equals" : "not equals");
    }

}

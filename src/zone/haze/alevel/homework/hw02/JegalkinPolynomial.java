package zone.haze.alevel.homework.hw02;

public class JegalkinPolynomial {
    private static byte positions = 8;
    private static byte x1 = 0b00001111;
    private static byte x2 = 0b00110011;
    private static byte x3 = 0b01010101;

    public static void main(String... args) {
        char[] vector = String
                .format("%" + positions + "s", Integer.toBinaryString(x1 & x2 | x2 & x3 | x1 & x3))
                .replace(' ', '0')
                .toCharArray();

        for (byte i = 0; i < positions; i++) {

            for (byte j = 0; j < positions - i; j++) {
                System.out.print(vector[j] + " ");

                if (j + 1 == positions) continue;

                vector[j] = (char) ((vector[j] - 0x30) ^ (vector[j + 1] - 0x30) + 0x30);
            }

            System.out.println();
            System.out.print(" ".repeat(i + 1));
        }
    }

}
